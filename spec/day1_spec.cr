require "./spec_helper"

describe Day1 do
  it "part1" do
    Day1.test1("+1, -2, +3, +1").should eq 3
    Day1.test1("+1, +1, +1").should eq 3
    Day1.test1("+1, +1, -2").should eq 0
    Day1.test1("-1, -2, -3").should eq -6
  end

  it "part2" do
    Day1.test2("+1, -2, +3, +1").should eq 2
    Day1.test2("+1, -1").should eq 0
    Day1.test2("+3, +3, +4, -2, -4").should eq 10
    Day1.test2("-6, +3, +8, +5, -6").should eq 5
    Day1.test2("+7, +7, -2, -7, -4").should eq 14
  end
end
