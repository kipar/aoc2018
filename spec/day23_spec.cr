require "./spec_helper"

describe Day23 do
  it "part1" do
    Day23.test1("pos=<0,0,0>, r=4
    pos=<1,0,0>, r=1
    pos=<4,0,0>, r=3
    pos=<0,2,0>, r=1
    pos=<0,5,0>, r=3
    pos=<0,0,3>, r=1
    pos=<1,1,1>, r=1
    pos=<1,1,2>, r=1
    pos=<1,3,1>, r=1").should eq 7
  end

  it "part2" do
    Day23.test2("").should eq "part2"
  end
end
