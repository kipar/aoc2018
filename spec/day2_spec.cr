require "./spec_helper"

describe Day2 do
  it "part1" do
    Day2.test1("
    abcdef
    bababc
    abbcde
    abcccd
    aabcdd
    abcdee
    ababab
    ").should eq 12
  end

  it "part2" do
    Day2.test2(
      "abcde
      fghij
      klmno
      pqrst
      fguij
      axcye
      wvxyz").should eq "fgij"
  end
end
