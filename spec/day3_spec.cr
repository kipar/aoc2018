require "./spec_helper"

describe Day3 do
  it "part1" do
    Day3.test1(
      "#1 @ 1,3: 4x4
      #2 @ 3,1: 4x4
      #3 @ 5,5: 2x2").should eq 4
    Day3.test1(
      "#1 @ 1,3: 4x4
    #2 @ 3,1: 4x4
    #3 @ 3,1: 4x4
    #4 @ 5,5: 2x2").should eq 16

    Day3.test1(
      "#1 @ 1,3: 4x4
       #2 @ 3,1: 4x4
       #3 @ 5,5: 2x2
       #4 @ 2,2: 2x2").should eq 6
    Day3.test1(
      "#1 @ 1,3: 4x4
      #3 @ 5,5: 2x2
      #4 @ 2,2: 2x7").should eq 8
    Day3.test1(
      "#1 @ 1,3: 4x4
        #3 @ 5,5: 2x2
        #3 @ 5,5: 2x3
        #4 @ 3,2: 2x7").should eq 12
    Day3.test1(
      "#1 @ 1,3: 4x4
      #3 @ 1,8: 1x1
      #3 @ 5,5: 2x2
        #3 @ 5,5: 2x3
        #4 @ 3,2: 2x7").should eq 12
    Day3.test1(
      "#1 @ 1,1: 7x7
      #3 @ 1,1: 7x1
      #3 @ 1,3: 7x1
      #3 @ 4,1: 1x9"
    ).should eq 14 + 7 - 2
    Day3.test1(
      "#3 @ 1,1: 7x1
        #3 @ 4,1: 1x7
        #1 @ 1,1: 7x7"
    ).should eq 14 - 1
  end

  it "part2" do
    Day3.test2(
      "#1 @ 1,3: 4x4
      #2 @ 3,1: 4x4
      #3 @ 5,5: 2x2"
    ).should eq 3
  end
end
