require "./spec_helper"

describe Day6 do
  it "part1" do
    Day6.test1(
      "1, 1
      1, 6
      8, 3
      3, 4
      5, 5
      8, 9").should eq 17
  end

  it "part2" do
    Day6.check_dist(
      "1, 1
    1, 6
    8, 3
    3, 4
    5, 5
    8, 9", 4, 3).should eq 30
    Day6.count_n(
      "1, 1
    1, 6
    8, 3
    3, 4
    5, 5
    8, 9", 32).should eq 16
  end
end
