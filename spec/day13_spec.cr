require "./spec_helper"

describe Day13 do
  it "part1" do
    map = <<-'MAP'
      |
      v
      |
      |
      |
      ^
      |
      MAP
    Day13.test1(map).should eq({0, 3})

    map = <<-'MAP'
      /->-\
      |   |  /----\
      | /-+--+-\  |
      | | |  | v  |
      \-+-/  \-+--/
        \------/
      MAP
    Day13.test1(map).should eq({7, 3})
  end

  it "part2" do
    map = <<-'MAP'
      />-<\  
      |   |  
      | /<+-\
      | | | v
      \>+</ |
        |   ^
        \<->/
      MAP
    Day13.test2(map).should eq({6, 4})
  end
end
