require "./spec_helper"

describe Day9 do
  it "part1" do
    Day9.test1("9 players; last marble is worth 25 points").should eq 32
    Day9.test1("10 players; last marble is worth 1618 points").should eq 8317
    Day9.test1("13 players; last marble is worth 7999 points").should eq 146373
    Day9.test1("17 players; last marble is worth 1104 points").should eq 2764
    Day9.test1("21 players; last marble is worth 6111 points").should eq 54718
    Day9.test1("30 players; last marble is worth 5807 points").should eq 37305
  end
end
