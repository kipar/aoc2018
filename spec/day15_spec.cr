require "./spec_helper"

describe Day15 do
  it "part1" do
    Day15.test1(<<-MAP
      #######   
      #.G...#
      #...EG#
      #.#.#G#
      #..G#E#
      #.....#   
      #######
      MAP
    ).should eq({47, 590, 27730})

    Day15.test1(<<-MAP
      #######
      #G..#E#
      #E#E.E#
      #G.##.#
      #...#E#
      #...E.#
      #######
      MAP
    ).should eq({37, 982, 36334})

    Day15.test1(<<-MAP
      #######
      #E..EG#
      #.#G.E#
      #E.##E#
      #G..#.#
      #..E#.#
      #######
      MAP
    ).should eq({46, 859, 39514})

    Day15.test1(<<-MAP
      #######
      #E.G#.#
      #.#G..#
      #G.#.G#
      #G..#.#
      #...E.#
      #######
      MAP
    ).should eq({35, 793, 27755})

    Day15.test1(<<-MAP
      #######
      #.E...#
      #.#..G#
      #.###.#
      #E#G#G#
      #...#G#
      #######
      MAP
    ).should eq({54, 536, 28944})

    Day15.test1(<<-MAP
      #########
      #G......#
      #.E.#...#
      #..##..G#
      #...##..#
      #...#...#
      #.G...G.#
      #.....G.#
      #########
      MAP
    ).should eq({20, 937, 18740})
  end

  it "part2" do
    Day15.test2(<<-MAP
      #######
      #.G...#
      #...EG#
      #.#.#G#
      #..G#E#
      #.....#
      #######
      MAP
    ).should eq({15, 4988})

    Day15.test2(<<-MAP
      #######
      #E..EG#
      #.#G.E#
      #E.##E#
      #G..#.#
      #..E#.#
      #######
      MAP
    ).should eq({4, 31284})

    Day15.test2(<<-MAP
      #######
      #E.G#.#
      #.#G..#
      #G.#.G#
      #G..#.#
      #...E.#
      #######
      MAP
    ).should eq({15, 3478})

    Day15.test2(<<-MAP
      #######
      #.E...#
      #.#..G#
      #.###.#
      #E#G#G#
      #...#G#
      #######
      MAP
    ).should eq({12, 6474})

    Day15.test2(<<-MAP
      #########
      #G......#
      #.E.#...#
      #..##..G#
      #...##..#
      #...#...#
      #.G...G.#
      #.....G.#
      #########
      MAP
    ).should eq({34, 1140})
  end
end
