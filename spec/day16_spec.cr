require "./spec_helper"

describe Day16 do
  it "part1" do
    Day16.test1("Before: [3, 2, 1, 1]
    9 2 1 2
    After:  [3, 2, 2, 1]").should eq 1
    Day16.test1("Before: [0, 0, 0, 0]
    9 0 0 0
    After:  [0, 0, 0, 0]").should eq 1
    Day16.test1("Before: [0, 0, 0, 0]
    9 0 0 10
    After:  [0, 0, 0, 0]").should eq 0
    Day16.test1("Before: [0, 10, 0, 0]
    9 1 10 3
    After:  [0, 10, 0, 10]").should eq 1
  end

  it "part2" do
    DayN.test2("").should eq "part2"
  end
end
