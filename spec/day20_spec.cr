require "./spec_helper"

describe Day20 do
  it "part1" do
    Day20.test1("^WNE$").should eq 3
    Day20.test1("^ENWWW(NEEE|SSE(EE|N))$").should eq 10
    Day20.test1("^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$").should eq 18
    Day20.test1("^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$").should eq 23
    Day20.test1("^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$").should eq 31
  end
end
