require "./spec_helper"

describe Day22 do
  it "part1" do
    Day22.erolevel(0, 0, 510).should eq 510
    (Day22::EROMAP[{1, 0}] = Day22.erolevel(1, 0, 510)).should eq 17317
    (Day22::EROMAP[{0, 1}] = Day22.erolevel(0, 1, 510)).should eq 8415
    Day22.erolevel(1, 1, 510).should eq 1805
    Day22.test1("depth: 510
    target: 10,10").should eq 114
  end

  it "part2" do
    Day22.test2("depth: 510
    target: 10,10").should eq 45
  end
end
