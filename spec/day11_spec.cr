require "./spec_helper"

describe Day11 do
  it "part1" do
    Day11.power(3, 5, 8).should eq 4
    Day11.power(122, 79, 57).should eq -5
    Day11.power(217, 196, 39).should eq 0
    Day11.power(101, 153, 71).should eq 4
    Day11.test1("18").should eq({33, 45, 29})
    Day11.test1("42").should eq({21, 61, 30})
  end

  pending "part2" do
    Day11.test2("18").should eq({90, 269, 16, 113})
    Day11.test2("42").should eq({232, 251, 12, 119})
  end
end
