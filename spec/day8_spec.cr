require "./spec_helper"

describe Day8 do
  it "part1" do
    Day8.test1("2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2").should eq 138
  end

  it "part2" do
    Day8.test2("2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2").should eq 66
  end
end
