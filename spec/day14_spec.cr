require "./spec_helper"

describe Day14 do
  it "part1" do
    Day14.test1("9").should eq "5158916779"
    Day14.test1("5").should eq "0124515891"
    Day14.test1("18").should eq "9251071085"
    Day14.test1("2018").should eq "5941429882"
  end

  it "part2" do
    Day14.test2("51589").should eq 9
    Day14.test2("01245").should eq 5
    Day14.test2("92510").should eq 18
    Day14.test2("59414").should eq 2018
  end
end
