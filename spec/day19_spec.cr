require "./spec_helper"

describe Day19 do
  it "part1" do
    Day19.test1("#ip 0
    seti 5 0 1
    seti 6 0 2
    addi 0 1 0
    addr 1 2 3
    setr 1 0 0
    seti 8 0 4
    seti 9 0 5").should eq 7
  end

  it "part2" do
    Day19.test2("#ip 0
    seti 5 0 1").should eq 1
  end
end
