require "./spec_helper"

describe Day12 do
  it "part1" do
    Day12.test1("initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #").should eq 325
  end

  it "part2" do
    data = "initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #"

    Day12.simulate_checked(data, 10).should eq({Day12.simulate(data, 10), false})
    Day12.simulate_checked(data, 1000).should eq({Day12.simulate(data, 1000), true})
  end
end
