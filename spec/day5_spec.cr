require "./spec_helper"

describe Day5 do
  it "part1" do
    Day5.test1("aA").should eq 0
    Day5.test1("abBA").should eq 0
    Day5.test1("abAB").should eq 4
    Day5.test1("aabAAB").should eq 6
    Day5.test1("dabAcCaCBAcCcaDA").should eq 10
  end

  it "part2" do
    Day5.test2("dabAcCaCBAcCcaDA").should eq 4
  end
end
