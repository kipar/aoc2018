require "./aoc2018"

module Day18
  include Day
  extend self

  enum Cell
    Open
    Tree
    Lumberyard
  end

  CHARS = {Cell::Open => '.', Cell::Tree => '|', Cell::Lumberyard => '#'}

  class Map
    getter data : Array(Cell)
    getter width : Int32
    getter height : Int32
    getter cache

    MAX_CACHE = 100

    def initialize(str)
      list = str.split("\n")
      @width = list[0].chomp.size
      @height = list.size
      @data = Array(Cell).new(width*height)
      list.each do |line|
        line.each_char do |char|
          next if char == '\r'
          @data << CHARS.key_for(char)
        end
      end
      @data_new = @data.clone
      @cache = Deque(Array(Cell)).new(MAX_CACHE)
      @cache << @data.clone
    end

    def read_order(x, y)
      x + @width*y
    end

    def count_neighbours(x, y, typ)
      n = 0
      (-1..1).each do |dx|
        (-1..1).each do |dy|
          ax = x + dx
          next unless (0...width).includes? ax
          ay = y + dy
          next unless (0...height).includes? ay
          next if dx == 0 && dy == 0
          n += 1 if @data[read_order(ax, ay)] == typ
        end
      end
      n
    end

    def step
      @data.each_with_index do |v, index|
        x = index % width
        y = index / width # /
        @data_new[index] = case v
                           when .open?
                             count_neighbours(x, y, Cell::Tree) >= 3 ? Cell::Tree : Cell::Open
                           when .tree?
                             count_neighbours(x, y, Cell::Lumberyard) >= 3 ? Cell::Lumberyard : Cell::Tree
                           when .lumberyard?
                             count_neighbours(x, y, Cell::Lumberyard) >= 1 && count_neighbours(x, y, Cell::Tree) >= 1 ? Cell::Lumberyard : Cell::Open
                           else raise ""
                           end
      end
      @data, @data_new = @data_new, @data
    end

    def simulate(nsteps)
      nsteps.times do |i|
        step
        @cache.each_with_index do |c, j|
          if c == @data
            old_tick = i - (@cache.size - j)
            new_tick = i
            cycle = new_tick - old_tick
            remains = (nsteps - new_tick - 1) % cycle
            remains.times { step }
            return true
          end
        end
        @cache.shift if @cache.size > MAX_CACHE
        @cache << @data.clone
      end
      false
    end

    def rate
      @data.count(&.tree?) * @data.count(&.lumberyard?)
    end

    def status
      Cell.values.map { |x| @data.count { |v| v == x } }
    end

    def draw
      puts ""
      puts(String.build do |str|
        @height.times do |y|
          @width.times do |x|
            str << CHARS[@data[read_order(x, y)]]
          end
          str << "\n"
        end
      end)
    end
  end

  def part1(input)
    map = Map.new(input.chomp)
    10.times { map.step }
    map.rate
  end

  def test1n(input, n)
    map = Map.new(input.chomp)
    n.times { map.step }
    map.rate
  end

  def test2n(input, n)
    map = Map.new(input.chomp)
    stopped = map.simulate(n)
    {map.rate, stopped}
  end

  def part2(input)
    test2n(input, 1000000000)[0]
  end
end
