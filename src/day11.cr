require "./aoc2018"

module Day11
  include Day
  extend self

  alias ItemType = Int32

  def power(x, y, id) : ItemType
    rack = (x + 10)
    level = (rack*y + id) * rack
    level = ((level / 100) % 10) - 5
    ItemType.new(level)
  end

  N = 300

  def add_sum(cells, data, n)
    (N - n).times do |y|
      (N - n).times do |x|
        v = ItemType.new(0)
        # .....
        # .XX2.
        # .XX2.
        # .111.
        # .....
        n.times { |i| v += cells[(x + i) + ((y + n - 1)*N)] }
        (n - 1).times { |i| v += cells[(x + n - 1) + ((y + i)*N)] }
        data[x + y*N] += v
      end
    end
  end

  def findbest(sum, n)
    best = {-1, -1}
    v = -10000
    (N - n).times do |y|
      (N - n).times do |x|
        if sum[x + y*N] > v
          v = sum[x + y*N]
          best = {x, y}
        end
      end
    end
    {best[0], best[1], v}
  end

  def part1(input)
    id = input.chomp.to_i
    cells = Array(ItemType).new(N*N) do |i|
      x = i % N
      y = i / N
      power(x, y, id)
    end
    sum = cells.clone
    add_sum(cells, sum, 2)
    add_sum(cells, sum, 3)
    findbest(sum, 3)
  end

  def getval(partsum, x, y, parti)
    partsum[x + y*(N - parti + 1)]
  end

  def findbestpart(sum, n)
    best = {-1, -1}
    bestv = -10000
    (N - n).times do |y|
      (N - n).times do |x|
        v = getval(sum, x, y, n)
        if v > bestv
          bestv = v
          best = {x, y}
        end
      end
    end
    {best[0], best[1], bestv}
  end

  def part2(input)
    id = input.chomp.to_i
    cells = Array(ItemType).new(N*N) do |i|
      x = i % N
      y = i / N
      power(x, y, id)
    end
    sums = {1 => cells}
    best = findbestpart(sums[1], 1)
    besti = 1
    (2..300).each do |i|
      if i % 2 == 0
        halfsum = sums[i/2]
        sum = Array(ItemType).new((N - i + 1)*(N - i + 1)) do |ipos|
          x = ipos % (N - i + 1)
          y = ipos / (N - i + 1)
          getval(halfsum, x, y, i/2) + getval(halfsum, x + i/2, y, i/2) + getval(halfsum, x, y + i/2, i/2) + getval(halfsum, x + i/2, y + i/2, i/2)
        end
      else
        halfsum1 = sums[i/2 + 1]
        halfsum = sums[i/2]
        sum = Array(ItemType).new((N - i + 1)*(N - i + 1)) do |ipos|
          # 11112222.
          # 11112222.
          # 11112222.
          # 11112222.
          # 333344444
          # 333344444
          # 333344444
          # 333344444
          # ....44444

          x = ipos % (N - i + 1)
          y = ipos / (N - i + 1)
          v = getval(halfsum, x, y, i/2) + getval(halfsum, x + i/2, y, i/2) + getval(halfsum, x, y + i/2, i/2) + getval(halfsum1, x + i/2, y + i/2, i/2 + 1)
          (i/2).times { |ay| v += getval(cells, x + i - 1, y + ay, 1) }
          (i/2).times { |ax| v += getval(cells, x + ax, y + i - 1, 1) }
          v
        end
      end

      sums[i] = sum
      v = findbestpart(sum, i)
      if v[2] > best[2]
        best = v
        besti = i
      end
    end
    {best[0], best[1], besti, best[2]}
  end
end
