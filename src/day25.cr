require "./aoc2018"

module Day25
  include Day
  extend self

  class Star
    @@uniqid = 1
    getter id = 0
    property! parent : Star
    getter coords : Array(Int32)

    def initialize(aline, alist)
      @id = @@uniqid
      @@uniqid += 1
      @coords = aline.split(',').map(&.to_i)
      @parent = self
      cur = self
      alist.each do |item|
        if close(item)
          cur.parent = item.find_root
          cur = cur.parent
        end
      end
      alist << self
    end

    def find_root
      root = self
      while root.parent != root
        root = root.parent
      end
      @parent = root
      root
    end

    def distance(other)
      coords.zip(other.coords).sum { |x, y| (x - y).abs }
    end

    def close(other)
      distance(other) <= 3
    end

    def to_s(io)
      io << "[#{@coords} => #{parent.id}]"
    end
  end

  def part1(input)
    stars = [] of Star
    input.chomp.split("\n").map { |x| Star.new(x, stars) }
    # puts "---"
    # puts stars.map(&.to_s).join("\n")
    # puts "---"
    stars.map(&.find_root).uniq.size
  end

  def part2(input)
    "part2"
  end
end
