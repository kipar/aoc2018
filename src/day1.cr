require "./aoc2018"

module Day1
  include Day
  extend self

  def to_list(input)
    input.chomp.split(/[,\n]/).map(&.chomp).map(&.to_i)
  end

  def part1(input)
    to_list(input).sum
  end

  def part2(input)
    was = Set(Int32).new
    v = 0
    loop do
      to_list(input).each do |x|
        was << v
        v += x
        return v if was.includes?(v)
      end
    end
  end
end
