require "./aoc2018"

module Day20
  include Day
  extend self

  record(Position, x : Int32, y : Int32, dist : Int32) do
    def +(pt : JustPoint)
      Position.new(@x + pt.x, @y + pt.y, dist + 1)
    end

    def point
      JustPoint.new(@x, @y)
    end

    def join(other_way : Int32)
      Position.new(@x, @y, {@dist, other_way}.min)
    end
  end

  record(JustPoint, x : Int32, y : Int32)

  DIRS = {
    'N' => JustPoint.new(0, -1),
    'S' => JustPoint.new(0, 1),
    'W' => JustPoint.new(-1, 0),
    'E' => JustPoint.new(1, 0),
  }

  def count_all(input)
    farthest = 0
    map = {} of JustPoint => Int32
    stack = Array(Position).new(input.size)
    cur = Position.new(0, 0, 0)
    input.each_char do |char|
      case char
      when '$'
        break
      when 'N', 'S', 'W', 'E'
        cur += DIRS[char]
        if old = map[cur.point]?
          cur = cur.join(old)
        else
          map[cur.point] = cur.dist
        end
        farthest = cur.dist if farthest < cur.dist
      when '^', '\n', '\r'
        # nothing
      when '('
        stack.push cur
      when '|'
        cur = stack.last
      when ')'
        cur = stack.pop
      else raise "wrong character: #{char}"
      end
    end
    {farthest, map.values.count { |x| x >= 1000 }}
  end

  def part1(input)
    count_all(input)[0]
  end

  def part2(input)
    count_all(input)[1]
  end
end
