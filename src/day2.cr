require "./aoc2018"

module Day2
  include Day
  extend self

  def part1(input)
    list = input.chomp.split("\n").map(&.strip)
    count2 = 0
    count3 = 0
    hash = Hash(Char, Int32).new(0)
    list.each do |s|
      hash.clear
      s.each_char do |c|
        hash[c] = hash[c] + 1
      end
      count2 += 1 if hash.values.includes? 2
      count3 += 1 if hash.values.includes? 3
    end
    count2*count3
  end

  def part2_bad(input)
    list = input.chomp.split("\n").map(&.strip)
    len = list[0].chars.size
    list.each_with_index do |s1, i|
      list.each_with_index do |s2, j|
        next if i >= j
        delta = 0
        index = 0
        thechar = 0
        s1.chars.zip(s2.chars).each do |c1, c2|
          if c1 != c2
            delta += 1
            thechar = index
          end
          index += 1
          break if delta > 2
        end
        return s1[0, thechar] + s1[thechar + 1, len] if delta == 1
      end
    end
    "???"
  end

  def part2(input)
    part2_bad(input)
  end
end
