require "./aoc2018"

module Day8
  include Day
  extend self

  class Node
    @children = [] of Node
    @meta = [] of Int32

    def parse(list, pos) : Int32
      n1 = list[pos]
      n2 = list[pos + 1]
      pos += 2
      n1.times do
        child = Node.new
        pos = child.parse(list, pos)
        @children << child
      end
      n2.times do |i|
        @meta << list[pos + i]
      end
      pos += n2
      pos
    end

    def sum_meta : Int32
      @meta.sum + @children.map { |x| x.sum_meta.as(Int32) }.sum
    end

    def value
      if @children.size == 0
        @meta.sum
      else
        @meta.map do |i|
          if i > 0 && i <= @children.size
            @children[i - 1].value.as(Int32)
          else
            0
          end
        end.sum
      end
    end
  end

  def part1(input)
    list = input.split(' ').map(&.to_i)
    root = Node.new
    n = root.parse(list, 0)
    raise "wrong data" unless n == list.size
    root.sum_meta
  end

  def part2(input)
    list = input.split(' ').map(&.to_i)
    root = Node.new
    n = root.parse(list, 0)
    raise "wrong data" unless n == list.size
    root.value
  end
end
