require "./aoc2018"

module Day10
  include Day
  extend self

  record(Point, x : Int32, y : Int32, vx : Int32, vy : Int32) do
    def initialize(prev : Point)
      @x = prev.x + prev.vx
      @y = prev.y + prev.vy
      @vx = prev.vx
      @vy = prev.vy
    end

    def initialize(text)
      raise "incorrect input: #{text}" unless m = /position=<(.*),(.*)> velocity=<(.*),(.*)>/.match(text)
      @x, @y, @vx, @vy = {m[1], m[2], m[3], m[4]}.map(&.to_i)
    end
  end

  class Map
    getter lights = [] of Point
    @area = [] of Bool
    @width = 0
    @height = 0

    def step
      lights.size.times do |i|
        lights[i] = Point.new(lights[i])
      end
    end

    def render
      minx, maxx = lights.map(&.x).minmax
      miny, maxy = lights.map(&.y).minmax
      @width = maxx - minx + 1
      @height = maxy - miny + 1
      return false unless @height < 25
      return false unless @width < lights.size / 2
      if @area.size < @width*@height
        # @area.capacity = @width*@height
        @area.clear
        (@width*@height).times do
          @area << false
        end
      else
        (@width*@height).times do |x|
          @area[x] = false
        end
      end
      lights.each do |pt|
        ax = pt.x - minx
        ay = pt.y - miny
        @area[ax + ay*@width] = true
      end
      return true
    end

    def print
      return "#{@width} x #{@height}" unless render
      String.build do |str|
        str << "\n"
        @height.times do |y|
          @width.times do |x|
            str << (pixel(x, y) ? "#" : ".")
          end
          str << "\n"
        end
      end
    end

    def pixel(ax, ay)
      @area[ax + ay*@width]
    end

    def check : Bool
      return false unless render
      @width.times do |x|
        @height.times do |y|
          next unless pixel(x, y)
          next if x > 0 && pixel(x - 1, y)
          next if x < @width - 1 && pixel(x + 1, y)
          next if y > 0 && pixel(x, y - 1)
          next if y < @height - 1 && pixel(x, y + 1)
          next if x > 0 && y > 0 && pixel(x - 1, y - 1)
          next if x > 0 && y < @height - 1 && pixel(x - 1, y + 1)
          next if x < @width - 1 && y > 0 && pixel(x + 1, y - 1)
          next if x < @width - 1 && y > @height - 1 && pixel(x + 1, y - 1)
          return false
        end
      end
      return true
    end

    def simulate
      loop do
        step
        break if check
      end
      print
    end

    def simulate_count
      n = 0
      loop do
        step
        n += 1
        break if check
      end
      n
    end
  end

  def part1(input)
    map = Map.new
    input.chomp.split("\n").map { |s| map.lights << Point.new(s) }
    map.simulate
  end

  def part2(input)
    map = Map.new
    input.chomp.split("\n").map { |s| map.lights << Point.new(s) }
    map.simulate_count
  end
end
