require "./aoc2018"

module Day24
  include Day
  extend self

  ELEMENTS = [] of String

  alias Element = Int32

  def string2element(astring) : Element
    el = ELEMENTS.index(astring)
    return el if el
    ELEMENTS << astring
    return ELEMENTS.size - 1
  end

  record(UnitType, hp : Int32, modifiers : Hash(Element, Int32), damage : Int32, element : Element, initiative : Int32) do
    def initialize(astring)
      m = /(?<hp>\d*) hit points (?:\((?<typ1>weak|immune) to (?<list1>[a-z, ]*)(?:; (?<typ2>weak|immune) to (?<list2>[a-z, ]*))?\) )?with an attack that does (?<damage>\d*) (?<element>[a-z]*) damage at initiative (?<initiative>\d*)/.match(astring)
      raise "incorrect group definition: #{astring}" unless m
      @hp, @damage, @initiative = {"hp", "damage", "initiative"}.map { |x| m[x].to_i }
      @element = Day24.string2element(m["element"])
      @modifiers = Hash(Element, Int32).new(1)
      (1..2).each do |i|
        if m["typ#{i}"]?
          change = m["typ#{i}"] == "weak" ? 2 : 0
          m["list#{i}"].split(", ").each do |item|
            @modifiers[Day24.string2element(item)] = change
          end
        end
      end
    end
  end

  enum Side
    ImmuneSystem
    Infection
  end

  class Group
    @owner : Battle
    getter typ : UnitType
    forward_missing_to @typ
    property count : Int32
    getter side : Side
    property targetted : Group?
    property target : Group?

    def initialize(@owner, @side, line)
      _, n, data = /(\d*) units each with (.*)/.match(line).not_nil!
      @count = n.to_i
      @typ = UnitType.new(data)
      @owner.side_counter[@side] += 1
    end

    def initialize(@owner, @typ, @count, @side)
    end

    def clone_for(other)
      Group.new(other, @typ, @count, @side)
    end

    def eff_power
      @count * (typ.damage + (@side.immune_system? ? @owner.boost : 0))
    end

    def ok_target?(x)
      x.side != side && !x.targetted && x.modifiers[element] > 0
    end

    def reset_target
      @targetted = nil
    end

    def select_target
      list = @owner.groups.select { |x| ok_target? x }
      t = list.empty? ? nil : list.max_by { |x| {x.modifiers[element], x.eff_power, x.initiative} }
      @target = t
      t.targetted = self if t
    end

    def dead
      @count <= 0
    end

    def attack
      return if dead
      return unless t = @target
      return if t.dead
      delta = t.modifiers[element] * eff_power / t.hp
      t.count -= delta
      @owner.any_hits = true if delta > 0
      @owner.side_counter[t.side] -= 1 if t.dead
    end
  end

  class Battle
    getter groups = [] of Group
    getter side_counter = Hash(Side, Int32).new(0)
    property boost = 0
    property any_hits = false

    def initialize(list)
      cur_sys = Side::ImmuneSystem
      list.each do |line|
        case line.chomp.strip
        when ""
          # nothing
        when "Immune System:"
          cur_sys = Side::ImmuneSystem
        when "Infection:"
          cur_sys = Side::Infection
        else
          @groups << Group.new(self, cur_sys, line.chomp)
        end
      end
      @groups.sort_by! { |x| -x.initiative }
    end

    def initialize(base : Battle)
      @side_counter = base.side_counter.clone
      @groups = base.groups.map { |x| x.clone_for(self) }
    end

    def clone
      Battle.new(self)
    end

    def turn
      @groups.each(&.reset_target)
      @groups.sort_by { |x| {-x.eff_power, -x.initiative} }.each(&.select_target)
      @groups.each(&.attack)
      @groups.reject!(&.dead)
    end

    def status
      Side.values.map { |v| @groups.select { |x| x.side == v }.sum(&.count) }
    end

    def fight
      loop do
        @any_hits = false
        turn
        break if side_counter.values.any?(&.==(0)) || !@any_hits
      end
    end
  end

  def part1(input)
    battle = Battle.new(input.split('\n'))
    battle.fight
    winner = battle.side_counter[Side::Infection] == 0 ? Side::ImmuneSystem : Side::Infection
    {winner, battle.groups.map(&.count).sum}
  end

  def part2(input)
    battle = Battle.new(input.split('\n'))
    min_boost = (0..1000000).bsearch do |v|
      b = battle.clone
      b.boost = v
      # p "checking #{v}..."
      b.fight
      winner = b.side_counter[Side::Infection] == 0 ? Side::ImmuneSystem : Side::Infection
      # p "checking #{v}: #{winner} #{b.groups.map(&.count).sum}"
      winner.immune_system?
    end
    battle.boost = min_boost.not_nil!
    battle.fight
    {min_boost, battle.groups.map(&.count).sum}
  end
end
