require "./aoc2018"

module Day13
  include Day
  extend self

  enum Dir
    North
    West
    South
    East
  end

  enum Turn
    Left     = -1
    Straight =  0
    Right    =  1
  end

  TURN_ORDER = {Turn::Left => Turn::Straight, Turn::Straight => Turn::Right, Turn::Right => Turn::Left}

  enum Road
    Empty
    Vertical
    Horizontal
    SlashTurn
    BackslachTurn
    Cross
  end

  ROAD_CHARS = {'/' => Road::SlashTurn, '\\' => Road::BackslachTurn, '|' => Road::Vertical, '-' => Road::Horizontal, '+' => Road::Cross, ' ' => Road::Empty}
  CART_CHARS = {'>' => Dir::West, '<' => Dir::East, '^' => Dir::North, 'v' => Dir::South}
  DIR_DELTA  = {Dir::North => {0, -1}, Dir::South => {0, 1}, Dir::East => {-1, 0}, Dir::West => {1, 0}}

  class Cart
    property x : Int32
    property y : Int32
    property dir : Dir
    property turn = Turn::Left
    property dead = false

    def initialize(@x, @y, @dir)
    end

    def rotate(dir, delta)
      Dir.new((dir.to_i + delta.to_i) % 4)
    end

    def step(road : Road)
      rot = case road
            when Road::SlashTurn
              (@dir.north? || @dir.south?) ? Turn::Right : Turn::Left
            when Road::BackslachTurn
              (@dir.north? || @dir.south?) ? Turn::Left : Turn::Right
            when Road::Cross
              @turn.tap { |x| @turn = TURN_ORDER[@turn] }
            else
              Turn::Straight
            end
      @dir = rotate(@dir, rot)
      dx, dy = DIR_DELTA[@dir]
      @x += dx
      @y += dy
    end
  end

  class Map
    @roads : Array(Road)
    getter carts : Array(Cart)
    @cartcache : Array(Cart?)
    @width : Int32
    @height : Int32

    def initialize(lines)
      @width = lines.max_of { |line| line.chars.size }
      @height = lines.size
      @roads = Array(Road).new(@width*@height, Road::Empty)
      @cartcache = Array(Cart?).new(@width*@height, nil)
      @carts = [] of Cart
      lines.each_with_index do |line, y|
        line.chars.each_with_index do |char, x|
          ipos = y*@width + x
          if ROAD_CHARS.has_key? char
            @roads[ipos] = ROAD_CHARS[char]
          elsif CART_CHARS.has_key? char
            c = Cart.new(x, y, CART_CHARS[char])
            @carts << c
            @cartcache[ipos] = c
            @roads[ipos] = (c.dir.north? || c.dir.south?) ? Road::Vertical : Road::Horizontal
          elsif char == '\r'
            next
          else
            raise "incorrect char in input: #{char}"
          end
        end
      end
    end

    def remove_dead_carts
      @carts.reject! do |c|
        if c.dead
          @cartcache[c.x + @width*c.y] = nil
          true
        else
          false
        end
      end
    end

    def draw
      puts ""
      puts(String.build do |str|
        @height.times do |y|
          @width.times do |x|
            ipos = x + y*@width
            if @cartcache[ipos]
              str << CART_CHARS.key_for(@cartcache[ipos].not_nil!.dir)
            else
              str << ROAD_CHARS.key_for(@roads[ipos].not_nil!)
            end
          end
          str << "\n"
        end
      end)
    end

    def step(safe) : Tuple(Int32, Int32)?
      @carts.sort_by! { |c| c.y * @width + c.x }
      @carts.each do |c|
        ipos = c.x + @width*c.y
        @cartcache[ipos] = nil
        r = @roads[ipos]
        c.step(r)
        ipos = c.x + @width*c.y
        if otherc = @cartcache[ipos]
          return {c.x, c.y} unless safe
          otherc.dead = true
          c.dead = true
        else
          @cartcache[ipos] = c
        end
      end
      remove_dead_carts if safe
      return nil
    end
  end

  def part1(input)
    map = Map.new(input.chomp.split("\n"))
    x = nil
    loop do
      x = map.step(false)
      break if x
    end
    x
  end

  def part2(input)
    map = Map.new(input.chomp.split("\n"))
    loop do
      map.step(true)
      break if map.carts.size <= 1
    end
    {map.carts[0].x, map.carts[0].y}
  end
end
