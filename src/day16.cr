require "./aoc2018"

module Day16
  include Day
  extend self

  alias MachWord = Int64

  alias CPU = StaticArray(MachWord, 4)

  enum OpCode
    Addr
    Addi
    Mulr
    Muli
    Banr
    Bani
    Borr
    Bori
    Setr
    Seti
    Gtir
    Gtri
    Gtrr
    Eqir
    Eqri
    Eqrr
  end

  CODETABLE = {} of Int32 => OpCode

  record(Operation, opcode : Int32, a : Int32, b : Int32, c : Int32) do
    setter opcode
  end

  def execute(before, op : Operation, acode : OpCode? = nil)
    result = before
    acode = CODETABLE[op.opcode] unless acode
    result[op.c] = MachWord.new(case acode
    when .addr?
      before[op.a] + before[op.b]
    when .addi?
      before[op.a] + op.b
    when .mulr?
      before[op.a] * before[op.b]
    when .muli?
      before[op.a] * op.b
    when .banr?
      before[op.a] & before[op.b]
    when .bani?
      before[op.a] & op.b
    when .borr?
      before[op.a] | before[op.b]
    when .bori?
      before[op.a] | op.b
    when .setr?
      before[op.a]
    when .seti?
      op.a
    when .gtir?
      op.a > before[op.b] ? 1 : 0
    when .gtri?
      before[op.a] > op.b ? 1 : 0
    when .gtrr?
      before[op.a] > before[op.b] ? 1 : 0
    when .eqir?
      op.a == before[op.b] ? 1 : 0
    when .eqri?
      before[op.a] == op.b ? 1 : 0
    when .eqrr?
      before[op.a] == before[op.b] ? 1 : 0
    else raise ""
    end)
    result
  end

  def determine(before, after : CPU, op : Operation) : Set(OpCode)
    result = Set(OpCode).new
    OpCode.values.each do |code|
      begin
        test = execute(before, op, code)
        result << code if after == test
      rescue exception
        # nothing
      end
    end
    result
  end

  def part1(input)
    before = [] of CPU
    after = [] of CPU
    ops = [] of Operation
    input.split("\n").each do |line|
      next if line.strip.size == 0
      if m = /((Before)|(After)): *\[(.*)\]/.match(line)
        list = m[4].split(',').map(&.to_i)
        dest = m[1] == "Before" ? before : after
        dest << CPU.new { |i| MachWord.new(list[i]) }
      else
        list = line.strip.split(' ').map(&.to_i)
        ops << Operation.new(list[0], list[1], list[2], list[3])
      end
    end
    n = 0
    ops.size.times do |i|
      n += 1 if determine(before[i], after[i], ops[i]).size >= 3
    end
    decode(input)
    n
  end

  def decode(input)
    before = [] of CPU
    after = [] of CPU
    ops = [] of Operation
    decodes = {} of Int32 => Set(OpCode)
    input.split("\n").each do |line|
      next if line.strip.size == 0
      if m = /((Before)|(After)): *\[(.*)\]/.match(line)
        list = m[4].split(',').map(&.to_i)
        dest = m[1] == "Before" ? before : after
        dest << CPU.new { |i| MachWord.new(list[i]) }
      else
        list = line.strip.split(' ').map(&.to_i)
        ops << Operation.new(list[0], list[1], list[2], list[3])
      end
    end
    n = 0
    ops.size.times do |i|
      possible = determine(before[i], after[i], ops[i])
      if decodes.has_key? ops[i].opcode
        decodes[ops[i].opcode] &= possible
      else
        decodes[ops[i].opcode] = possible
      end
    end
    OpCode.values.size.times do
      if v = decodes.find { |k, v| v.size == 1 }
        num = v[0]
        op = v[1].to_a[0]
        CODETABLE[num] = op
        decodes.delete num
        decodes.each { |k, v| v.delete(op) }
      else
        break
      end
    end
    CODETABLE
  end

  def part2(input)
    cpu = CPU.new(0)
    input.chomp.split("\n").each do |line|
      next if line.strip.size == 0
      list = line.strip.split(' ').map(&.to_i)
      cpu = execute(cpu, Operation.new(list[0], list[1], list[2], list[3]))
    end
    cpu[0]
  end
end
