require "./aoc2018"

module Day15
  include Day
  extend self

  enum CellType
    Wall
    Floor
  end

  enum Side
    Elf
    Goblin
  end

  alias WavePoint = {d: Int32, sources: Int32}
  alias Point = {Int32, Int32}

  def self.enemy(side) : Side
    side == Side::Elf ? Side::Goblin : Side::Elf
  end

  record(Option, pt : Point, wp : WavePoint) do
    include Comparable(Option)

    def <=>(other : Option)
      a = wp[:d] <=> other.wp[:d]
      return a if a != 0
      if wp[:sources] != other.wp[:sources]
        32.times do |i|
          aa = (wp[:sources] & (1 << i))
          bb = (other.wp[:sources] & (1 << i))
          return -1 if aa > 0 && bb == 0
          return 1 if aa == 0 && bb > 0
          break if aa > 0 && bb > 0
        end
      end
      a = pt[1] <=> other.pt[1]
      return a if a != 0
      pt[0] <=> other.pt[0]
    end
  end

  class Unit
    getter map : Map
    property x : Int32
    property y : Int32
    getter side : Side
    property hp = 200
    getter damage = 3
    property was_read_order = 0
    getter id : Char

    def distance(tox, toy)
      (@x - tox).abs + (@y - toy).abs
    end

    def read_order
      @x + @y*@map.width
    end

    def initialize(@map, @side, @x, @y)
      n = @map.units[@side].size
      @id = @side.elf? ? ('1'.ord + n).chr : ('A'.ord + n).chr
    end

    def die
      # puts "#{inspect} dies" if @map.tick == 69
      @map.units[@side].delete(self)
      @map.winner = Day15.enemy(@side) if @map.units[@side].size == 0
      @map.cells[read_order] = CellType::Floor
    end

    def take_hit
      @damage = @map.elf_damage if @side.goblin?
      # puts "#{inspect} was hit" if @map.tick == 69
      @hp -= @damage
      die if @hp <= 0
    end

    def move(tox, toy)
      @map.cells[read_order] = CellType::Floor
      @x = tox
      @y = toy
      @map.cells[read_order] = self
    end

    def attack
      close = map.units[Day15.enemy(@side)].select do |u|
        distance(u.x, u.y) == 1
      end
      if close.size > 0
        # puts "#{inspect} hits" if @map.tick == 69
        close.min_by { |u| u.hp*map.width*map.height + u.read_order }.take_hit
        return true
      end
      false
    end

    def process
      return if @hp <= 0
      if @map.winner == @side
        @map.game_over = true
        return
      end
      return if attack
      map.build_wave(Day15.enemy(@side))
      options = [] of Option
      map.neighbours(@x, @y) do |ax, ay|
        wp = map.wave[map.read_order(ax, ay)]
        next if wp[:d] < 0
        options << Option.new({ax, ay}, wp)
      end
      # puts "#{@side} steps #{options.size}"
      return if options.size == 0
      best = options.min
      move(*best.pt)
      attack
    end

    def inspect(io)
      io << side
      io << id
      io << "("
      io << hp
      io << ")"
    end
  end

  class Map
    getter cells
    getter units
    getter width : Int32
    getter height : Int32
    getter tick = 0
    property winner : Side?
    property game_over = false
    property elf_damage = 3

    getter! wave
    getter! wave_targets

    def initialize(@width, @height, acells)
      @units = Hash(Side, Array(Unit)).new
      @units[Side::Elf] = [] of Unit
      @units[Side::Goblin] = [] of Unit
      @cells = Array(CellType | Unit).new(@width * @height)
      acells.each_with_index do |item, i|
        case item
        when CellType
          @cells << item
        when Unit
          side = item.side
          x = i % @width
          y = i / @width
          guy = Unit.new(self, side, x, y)
          @cells << guy
          @units[side] << guy
        end
      end
      @wave = Array(WavePoint).new(@width * @height, {d: -1, sources: 0})
      @wave_targets = Array(Point).new(@units.values.max_of(&.size)*4)
    end

    def clone
      Map.new(@width, @height, @cells)
    end

    def initialize(list)
      @units = Hash(Side, Array(Unit)).new
      @units[Side::Elf] = [] of Unit
      @units[Side::Goblin] = [] of Unit
      @width = list.max_of { |s| s.strip.size }
      @height = list.size
      @cells = Array(CellType | Unit).new(@width * @height)
      list.each_with_index do |line, y|
        line.chars.each_with_index do |sym, x|
          case sym
          when '.'
            @cells << CellType::Floor
          when '#'
            @cells << CellType::Wall
          when 'E', 'G'
            side = sym == 'E' ? Side::Elf : Side::Goblin
            guy = Unit.new(self, side, x, y)
            @cells << guy
            @units[side] << guy
          when '\n', '\r', ' '
            next
          else
            raise "incorrect symbol: #{sym}"
          end
        end
      end
      raise "Map is incorrect: #{@cells.size}" if @cells.size < @width*@height
      raise "too much units" if @units.values.any? { |v| v.size > 32 }
      @wave = Array(WavePoint).new(@width * @height, {d: -1, sources: 0})
      @wave_targets = Array(Point).new(@units.values.max_of(&.size)*4)
    end

    def read_order(x, y)
      x + @width*y
    end

    def neighbours(x, y, &block)
      yield(x + 0, y - 1) if y > 0 && cells[read_order(x + 0, y - 1)] == CellType::Floor
      yield(x - 1, y + 0) if x > 0 && cells[read_order(x - 1, y + 0)] == CellType::Floor
      yield(x + 1, y + 0) if x < @width - 1 && cells[read_order(x + 1, y + 0)] == CellType::Floor
      yield(x + 0, y + 1) if y < @height - 1 && cells[read_order(x + 0, y + 1)] == CellType::Floor
    end

    def build_wave(aside)
      wave.fill({d: -1, sources: 0})
      wave_targets.clear
      @units[aside].each do |u|
        neighbours(u.x, u.y) do |ax, ay|
          wave_targets << {ax, ay}
        end
      end
      wave_targets.sort_by! { |(ax, ay)| read_order(ax, ay) }
      wave_targets.each_with_index { |(ax, ay), i| wave[read_order(ax, ay)] = {d: 0, sources: (1 << i)} }
      changed = true
      curd = 0
      while changed
        changed = false
        curd += 1
        @height.times do |y|
          @width.times do |x|
            next unless @cells[read_order(x, y)] == CellType::Floor
            next if wave[read_order(x, y)][:d] >= 0
            mind = nil
            neighbours(x, y) do |fx, fy|
              d = wave[read_order(fx, fy)][:d]
              next if d < 0 || d == curd
              mind = d if !mind || mind > d
            end
            next unless mind
            s = 0
            neighbours(x, y) do |fx, fy|
              d = wave[read_order(fx, fy)][:d]
              next if d != mind
              s = s | wave[read_order(fx, fy)][:sources]
            end
            wave[read_order(x, y)] = {d: mind + 1, sources: s}
            changed = true
          end
        end
      end
    end

    def rate
      @units[@winner.not_nil!].sum(&.hp)
    end

    def turn
      list = @units.values.flatten.sort_by(&.read_order)
      # pp list.map(&.id)
      list.each { |u| u.was_read_order = u.read_order }
      list.each { |u| u.process; return if @game_over }
      @tick += 1
      # p "#{@tick}: #{@units}"
    end

    def draw
      puts ""
      puts(String.build do |str|
        @height.times do |y|
          @width.times do |x|
            cell = @cells[read_order(x, y)]
            sym = case cell
                  when CellType::Floor
                    "."
                  when CellType::Wall
                    "#"
                  when Unit
                    cell.id
                  end
            str << sym
          end
          str << "\n"
        end
      end)
    end
  end

  def part1(input)
    list = input.chomp.split('\n')
    map = Map.new(list)
    loop do
      map.turn
      # map.draw if map.tick == 69
      break if map.game_over
      # break if map.tick > 3
    end
    # pp map.units
    {map.tick, map.rate, map.tick*map.rate}
  end

  def simulate(amap, n)
    map = amap.clone
    nelves = map.units[Side::Elf].size
    map.elf_damage = n
    loop do
      map.turn
      # map.draw if map.tick == 69
      break if map.game_over || map.units[Side::Elf].size < nelves
    end
    return nil unless map.winner == Side::Elf
    map.rate*map.tick
  end

  def part2(input)
    list = input.chomp.split('\n')
    amap = Map.new(list)
    cur = 3
    while !simulate(amap, cur)
      cur = cur*2
    end
    anoutcome = 0
    weapon = ((cur/2 + 1)..cur).bsearch do |i|
      v = simulate(amap, i)
      anoutcome = v if v
      v
    end
    {weapon, simulate(amap, weapon.not_nil!)}
  end
end
