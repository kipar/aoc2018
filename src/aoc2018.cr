require "benchmark"

module Day
  def run
    result = ""
    name = "./input/#{self.to_s.downcase}"
    input = File.read(name)
    time = Time.measure { result = part1(input) }
    us = time.microseconds + time.seconds*1000000
    puts "#{self}.part1: #{result.to_s.ljust(30)} #{us}"

    name2 = "./input/#{self.to_s.downcase}.2"
    if File.exists?(name2)
      input = File.read(name2)
    end
    time = Time.measure { result = part2(input) }
    us = time.microseconds + time.seconds*1000000
    puts "#{self}.part2: #{result.to_s.ljust(30)} #{us}"
  end

  def test1(input)
    self.part1(input)
  end

  def test2(input)
    self.part2(input)
  end
end
