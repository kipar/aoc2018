require "./aoc2018"

module Day9
  include Day
  extend self

  class CircleNode
    property! next : CircleNode
    property! prev : CircleNode
    getter score : Int32

    def initialize(@score, *, after_what)
      @next = after_what.next
      @prev = after_what
      after_what.next.prev = self
      after_what.next = self
    end

    def initialize(@score)
      @prev = self
      @next = self
    end

    def scroll(index)
      r = self
      if index > 0
        index.times { r = r.next }
      else
        (-index).times { r = r.prev }
      end
      r
    end

    def drop
      prev.next = self.next
      self.next.prev = prev
    end
  end

  def play_game(nplayers, maxindex)
    index = 0
    scores = Array(Int64).new(nplayers, 0)
    current = CircleNode.new(index)
    curplayer = 0
    loop do
      index += 1
      if index % 23 == 0
        scores[curplayer] += index
        node = current.scroll(-7)
        scores[curplayer] += node.score
        node.drop
        current = node.next
      else
        current = current.scroll(1)
        current = CircleNode.new(index, after_what: current)
      end
      curplayer = (curplayer + 1) % nplayers
      break if index >= maxindex
    end
    scores.max
  end

  def part1(input)
    raise "incorrect input" unless m = /(\d*) players; last marble is worth (\d*) points/.match(input)
    nplayers, maxindex = {m[1], m[2]}.map(&.to_i)
    play_game nplayers, maxindex
  end

  def part2(input)
    raise "incorrect input" unless m = /(\d*) players; last marble is worth (\d*) points/.match(input)
    nplayers, maxindex = {m[1], m[2]}.map(&.to_i)
    play_game nplayers, maxindex*100
  end
end
