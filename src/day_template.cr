require "./aoc2018"

module DayN
  include Day
  extend self

  def part1(input)
    "part1"
  end

  def part2(input)
    "part2"
  end
end
