require "./aoc2018"

module Day6
  include Day
  extend self

  class Map
    @data : Array(Int32)
    @roots = [] of NamedTuple(id: Int32, x: Int32, y: Int32)
    @width : Int32
    @height : Int32

    def initialize(@x1 : Int32, @x2 : Int32, @y1 : Int32, @y2 : Int32)
      @width = @x2 - @x1 + 1
      @height = @y2 - @y1 + 1
      @data = Array(Int32).new(@width * @height, 0)
    end

    def set_pixel(x, y, v)
      @data[x + y*@width] = v
    end

    def get_pixel(x, y)
      @data[x + y*@width]
    end

    def add_root(id, x, y)
      @roots << {id: id, x: x - @x1, y: y - @y1}
      set_pixel x - @x1, y - @y1, id
    end

    def distance(x1, y1, x2, y2)
      (x1 - x2).abs + (y1 - y2).abs
    end

    def fill
      @width.times do |x|
        @height.times do |y|
          v = @roots.map { |root| {id: root[:id], dist: distance(root[:x], root[:y], x, y)} }
          best = v.min_by { |x| x[:dist] }
          if v.count { |x| x[:dist] == best[:dist] } > 1
            set_pixel x, y, 0
          else
            set_pixel x, y, best[:id]
          end
        end
      end
    end

    def limited?(n)
      @width.times do |x|
        return false if get_pixel(x, 0) == n
        return false if get_pixel(x, @height - 1) == n
      end
      @height.times do |y|
        return false if get_pixel(0, y) == n
        return false if get_pixel(@width - 1, y) == n
      end
      return true
    end

    def area(n)
      @data.count(n)
    end

    def max_limited
      (1..@roots.size + 1).to_a.select { |n| limited? n }.map { |n| area n }.max
    end

    def print
      puts ""
      @height.times do |y|
        s = String.build do |str|
          @width.times do |x|
            str << get_pixel(x, y).to_s
          end
        end
        puts s
      end
    end

    def sum_distance(x, y)
      @roots.sum { |r| distance(x, y, r[:x], r[:y]) }
    end

    def count_n(maxn)
      n = 0
      @width.times do |x|
        @height.times do |y|
          if sum_distance(x, y) < maxn
            raise "margin too small" if x == 0 || x == @width - 1 || y == 0 || y == @height - 1
            n += 1
          end
        end
      end
      n
    end
  end

  MARGIN = 0

  def part1(input)
    list = input.chomp.split("\n").map { |s| s.split(',').map(&.to_i) }
    xmin = list.map { |x| x[0] }.min
    xmax = list.map { |x| x[0] }.max
    ymin = list.map { |x| x[1] }.min
    ymax = list.map { |x| x[1] }.max
    map = Map.new(xmin - MARGIN, xmax + MARGIN, ymin - MARGIN, ymax + MARGIN)
    list.each_with_index do |v, i|
      map.add_root i + 1, v[0], v[1]
    end
    map.fill
    # map.print
    map.max_limited
  end

  def check_dist(input, x, y)
    list = input.chomp.split("\n").map { |s| s.split(',').map(&.to_i) }
    xmin = list.map { |x| x[0] }.min
    xmax = list.map { |x| x[0] }.max
    ymin = list.map { |x| x[1] }.min
    ymax = list.map { |x| x[1] }.max

    map = Map.new(0, 10, 0, 10)
    list.each_with_index do |v, i|
      map.add_root i + 1, v[0], v[1]
    end
    map.sum_distance(x, y)
  end

  def count_n(input, n)
    list = input.chomp.split("\n").map { |s| s.split(',').map(&.to_i) }
    xmin = list.map { |x| x[0] }.min
    xmax = list.map { |x| x[0] }.max
    ymin = list.map { |x| x[1] }.min
    ymax = list.map { |x| x[1] }.max

    map = Map.new(xmin - MARGIN, xmax + MARGIN, ymin - MARGIN, ymax + MARGIN)
    list.each_with_index do |v, i|
      map.add_root i + 1, v[0], v[1]
    end
    map.count_n(n)
  end

  def part2(input)
    count_n(input, 10000)
  end
end
