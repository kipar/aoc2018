require "./aoc2018"

module Day3
  include Day
  extend self

  enum IntersectType
    No
    Inside
    Contain
  end

  record(Rect, id : Int32, x : Int32, y : Int32, w : Int32, h : Int32) do
    def initialize(astring)
      raise "incorrect claim" unless m = /#(.*)@(.*),(.*):(.*)x(.*)/.match(astring)
      @id, @x, @y, @w, @h = {m[1], m[2], m[3], m[4], m[5]}.map(&.to_i)
    end

    def x2
      @x + @w - 1
    end

    def y2
      @y + @h - 1
    end

    def area
      @w*@h
    end

    def fully_contains?(other : Rect)
      y2 >= other.y2 && y <= other.y && x2 >= other.x2 && x <= other.x
    end

    def check(other : Rect) : (IntersectType | Rect)
      return IntersectType::No if other.x2 < x
      return IntersectType::No if x2 < other.x
      return IntersectType::No if other.y2 < y
      return IntersectType::No if y2 < other.y
      # return IntersectType::Contain if self.fully_contains?(other)
      return IntersectType::Inside if other.fully_contains?(self)
      ax = {x, other.x}.max
      ax2 = {x2, other.x2}.min
      ay = {y, other.y}.max
      ay2 = {y2, other.y2}.min
      Rect.new(other.id, ax, ay, ax2 - ax + 1, ay2 - ay + 1)
    end
  end

  class MyAABB
    @@singles = Set(Int32).new
    getter leaves = [] of MyAABB
    getter vert_split : Bool
    getter pos : Rect
    @value : Int32 = 0
    property id : Int32 = 0
    @owner : MyAABB?

    def initialize(@pos, @vert_split, @owner = nil)
    end

    def add_leaf(c1, c2)
      if @vert_split
        leaf = MyAABB.new(Rect.new(0, c1, pos.y, c2 - c1 + 1, pos.h), !vert_split, self)
      else
        leaf = MyAABB.new(Rect.new(0, pos.x, c1, pos.w, c2 - c1 + 1), !vert_split, self)
      end
      leaf.id = id
      @leaves << leaf
      leaf
    end

    def inc
      @value += 1
      @@singles.delete(id) if self.value > 1
    end

    def value
      aowner = @owner
      @value + (aowner ? aowner.value : 0)
    end

    def add(r : Rect)
      # return if value >= 2
      # r is fully inside me by contract
      if leaves.size == 0
        if vert_split
          my_c1 = pos.x
          my_c2 = pos.x2
          it_c1 = r.x
          it_c2 = r.x2
        else
          my_c1 = pos.y
          my_c2 = pos.y2
          it_c1 = r.y
          it_c2 = r.y2
        end
        add_leaf(my_c1, it_c1 - 1) if it_c1 > my_c1
        add_leaf(it_c1, it_c2)
        add_leaf(it_c2 + 1, my_c2) if it_c2 < my_c2
      end
      leaves.each do |leaf| # TODO - could do better
        case over = leaf.pos.check(r)
        # when IntersectType::No
        # nothing
        when IntersectType::Inside
          leaf.inc
          leaf.id = r.id
          @@singles.delete(r.id) if leaf.value > 1
        when Rect
          leaf.add(over)
        end
      end
    end

    def measure
      return pos.area if value >= 2
      @leaves.map(&.measure).sum
    end

    def reset_singles
      @@singles.clear
    end

    def add_single(x)
      @@singles.add(x)
    end

    def find_single
      raise "#{@@singles.size} singles instead of one" if @@singles.size != 1
      @@singles.to_a[0]
    end

    def draw(map, scale)
      if value > 1
        (pos.x..pos.x2).each do |x|
          (pos.y..pos.y2).each do |y|
            map[y/scale][x/scale] = value.to_s unless x/scale >= map.size || y/scale >= map.size
          end
        end
      end
      @leaves.each(&.draw(map, scale))
    end
  end

  def part1(input)
    list = input.chomp.split("\n").map { |x| Rect.new(x) }
    node = MyAABB.new(Rect.new(0, 0, 0, 1001, 1001), false)
    list.each do |r|
      raise "" unless node.pos.fully_contains? r
      node.add(r)
    end
    node.measure
  end

  def part2(input)
    list = input.chomp.split("\n").map { |x| Rect.new(x) }
    node = MyAABB.new(Rect.new(0, 0, 0, 1001, 1001), false)
    node.reset_singles
    list.each do |r|
      raise "" unless node.pos.fully_contains? r
      node.add_single(r.id)
      node.add(r)
    end
    node.find_single
  end
end
