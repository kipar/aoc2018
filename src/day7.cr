require "./aoc2018"

module Day7
  include Day
  extend self

  class Machine
    @open = Set(String).new
    @prereq = {} of String => Set(String)
    @cur_prereq = {} of String => Set(String)

    def clear
      @prereq.clear
    end

    def reset
      @open.clear
      @cur_prereq = @prereq.clone
      @prereq.each do |s, value|
        if value.size == 0
          @cur_prereq.delete s
          @open << s
        end
      end
    end

    def add_line(line)
      raise "incorrect line" unless m = /Step (.*) must be finished before step (.*) can begin./.match(line)
      afrom, ato = m[1], m[2]
      @prereq[afrom] = Set(String).new unless @prereq.has_key? afrom
      @prereq[ato] = Set(String).new unless @prereq.has_key? ato
      @prereq[ato] << afrom
    end

    def step : String?
      take.tap { |item| done(item) if item }
    end

    def work
      reset
      String.build do |str|
        while s = step
          str << s
        end
      end
    end

    def take : String?
      return nil if @open.size == 0
      @open.min.tap { |selected| @open.delete selected }
    end

    def done(selected)
      check = @cur_prereq.keys
      check.each do |item|
        aset = @cur_prereq[item]
        aset.delete selected
        if aset.size == 0
          @open << item
          @cur_prereq.delete item
        end
      end
    end

    def time_for(item)
      item.chars[0].ord - 'A'.ord + 1
    end

    def multi_work(nworkers, ndelta)
      reset
      tick = 0
      work_delays = Array(Int32).new(nworkers, 0)
      work_items = Array(String?).new(nworkers, nil)
      while @cur_prereq.size > 0 || @open.size > 0 || work_items.any?
        # process working
        nworkers.times do |i|
          if work_delays[i] > 0
            work_delays[i] -= 1
          elsif work_items[i]
            # p "tick #{tick}: worker #{i} finished #{work_items[i]}"
            done(work_items[i])
            work_items[i] = nil
          end
        end
        # take new items
        nworkers.times do |i|
          unless work_items[i]
            mine = take
            work_items[i] = mine
            if mine
              # p "tick #{tick}: worker #{i} took #{mine}"
              work_delays[i] = ndelta + time_for(mine) - 1
            end
          end
        end
        tick += 1
      end
      tick - 1
    end
  end

  def part1(input)
    cpu = Machine.new
    input.chomp.split("\n").each do |s|
      cpu.add_line s
    end
    cpu.work
  end

  def test_part2(input, nworkers, ndelta)
    cpu = Machine.new
    input.chomp.split("\n").each do |s|
      cpu.add_line s
    end
    cpu.multi_work(nworkers, ndelta)
  end

  def part2(input)
    test_part2(input, 5, 60)
  end
end
