require "./aoc2018"

struct StaticArray(T, N)
  def +(other : StaticArray(T, N))
    StaticArray(T, N).new { |i| self[i] + other[i] }
  end

  def -(other : StaticArray(T, N))
    StaticArray(T, N).new { |i| self[i] - other[i] }
  end

  def *(k)
    self.map { |v| v*k }
  end
end

module Day19
  include Day
  extend self

  alias CPU = StaticArray(Int64, 6)

  DECODES = Day16::OpCode.values.map { |x| ({x.to_s.downcase, x}) }.to_h

  def parse(str) : Day16::Operation
    _, instr, op1, op2, op3 = /(....) (\d*) (\d*) (\d*)/.match(str).not_nil!
    Day16::Operation.new(DECODES[instr].to_i, op1.to_i, op2.to_i, op3.to_i)
  end

  def part1(input)
    list = input.chomp.split("\n")
    first = list.shift
    _, jump_reg = /#ip (\d*)/.match(first).not_nil!
    jump_reg = jump_reg.to_i
    program = list.map { |str| parse(str) }

    cpu = CPU.new(0)
    loop do
      break unless (0...program.size).includes? cpu[jump_reg]
      op = program[cpu[jump_reg]]
      # pp cpu.map(&.to_i32)
      cpu = Day16.execute(cpu, op, Day16::OpCode.new(op.opcode))
      cpu[jump_reg] += 1
    end

    cpu[0]
  end

  def simulate_steps(cpu, program, jump_reg, n)
    n.times do
      return nil unless (0...program.size).includes? cpu[jump_reg]
      op = program[cpu[jump_reg]]
      # pp cpu.map(&.to_i32)
      cpu = Day16.execute(cpu, op, Day16::OpCode.new(op.opcode))
      cpu[jump_reg] += 1
    end
    cpu
  end

  BIG_N   = 100_000_000
  SMALL_N =           2

  def try_optimize(cpu, prev_state, program, jump_reg, tick)
    delta = cpu - prev_state[0]
    nonzero = delta.count(&.!=(0))
    return nil if nonzero != 1
    loop_time = tick - prev_state[1]
    old = cpu
    cpu = simulate_steps(cpu, program, jump_reg, loop_time*SMALL_N)
    return nil unless cpu
    tick += loop_time*SMALL_N
    return nil if cpu - old != delta*SMALL_N
    puts "loop detected, time #{loop_time} cpu #{cpu}"
    bifur = (SMALL_N..BIG_N).bsearch do |k|
      cpu2 = cpu + delta*k
      cpu3 = simulate_steps(cpu2, program, jump_reg, loop_time)
      cpu3.nil? || (cpu3 - cpu2 != delta)
    end
    bifur = BIG_N unless bifur
    tick += (bifur - 1)*loop_time
    cpu += delta*(bifur - 1)
    puts "bifurcation at #{bifur}"
    {cpu, tick}
  end

  def part2(input)
    list = input.chomp.split("\n")
    first = list.shift
    _, jump_reg = /#ip (\d*)/.match(first).not_nil!
    jump_reg = jump_reg.to_i
    program = list.map { |str| parse(str) }

    cache = {} of Int32 => {CPU, Int64}

    cpu = CPU.new(0)
    cpu[0] = 1_i64
    tick = 0_i64
    loop do
      break unless (0...program.size).includes? cpu[jump_reg]
      pos = cpu[jump_reg].to_i
      if prev_state = cache[pos]?
        if anew = try_optimize(cpu, prev_state, program, jump_reg, tick)
          cache.clear
          cpu, tick = anew
        end
      end
      cache[pos] = {cpu, tick}
      op = program[pos]
      # pp cpu.map(&.to_i32)
      cpu = Day16.execute(cpu, op, Day16::OpCode.new(op.opcode))
      tick += 1
      cpu[jump_reg] += 1
    end

    cpu[0]
  end
end
