require "./aoc2018"

module Day17
  include Day
  extend self

  record(Line, x1 : Int32, y1 : Int32, x2 : Int32, y2 : Int32, vertical : Bool) do
    def initialize(s)
      if m = /y=(\d*), x=(\d*)..(\d*)/.match(s)
        @y1 = @y2 = m[1].to_i
        @x1 = m[2].to_i
        @x2 = m[3].to_i
        @vertical = false
      elsif m = /x=(\d*), y=(\d*)..(\d*)/.match(s)
        @x1 = @x2 = m[1].to_i
        @y1 = m[2].to_i
        @y2 = m[3].to_i
        @vertical = true
      else
        raise "incorrect input: #{s}"
      end
    end

    def draw(&block)
      if vertical
        (y1..y2).each { |y| yield(@x1, y) }
      else
        (x1..x2).each { |x| yield(x, @y1) }
      end
    end
  end

  enum CellType
    Empty
    Rock
    WaterRest
    WaterFlow
  end

  enum FillStatus
    Continues
    Drop
    Stop
  end

  class Flow
    property cur_x : Int32
    property cur_y : Int32
    getter owner : Map
    @dead = false

    def initialize(@owner, @cur_x, @cur_y)
      @owner.flows << self
    end

    def simulate
      return if @cur_y >= @owner.height
      # flow down to the obstracle
      y = @cur_y
      x = @cur_x
      cell = CellType::Empty
      loop do
        y += 1
        break if y >= @owner.height
        cell = @owner.cell_at(x, y)
        break if cell != CellType::Empty
      end
      # draw vertical line
      (@cur_y...y).each { |ay| @owner.set_cell(x, ay, CellType::WaterFlow) }
      return if y >= @owner.height
      y -= 1
      return if cell.water_flow?
      # fill closed spaces
      loop do
        sides = {-1, 1}.map do |dx|
          ax = x
          status = FillStatus::Continues
          while status.continues?
            ax += dx
            status = @owner.check_flow(ax, y)
          end
          ax -= dx if status.stop?
          {x: ax, status: status}
        end
        last_line = sides.any? { |side| side[:status].drop? }
        filler = last_line ? CellType::WaterFlow : CellType::WaterRest
        (sides[0][:x]..sides[1][:x]).each { |ax| @owner.set_cell(ax, y, filler) }
        if last_line
          sides.each do |side|
            if side[:status].drop?
              Flow.new(@owner, side[:x], y)
            end
          end
          return
        end
        y = y - 1
      end
    end
  end

  CELL_SYM = {CellType::Empty     => '.',
              CellType::Rock      => '#',
              CellType::WaterRest => '~',
              CellType::WaterFlow => '|'}

  class Map
    getter width : Int32
    getter height : Int32
    getter minx : Int32
    getter maxx : Int32
    getter miny : Int32
    getter maxy : Int32
    @data : Array(CellType)
    getter flows = [] of Flow

    def set_cell(ax, ay, acell)
      @data[ax + ay*@width] = acell
    end

    def cell_at(ax, ay)
      @data[ax + ay*@width]
    end

    def check_flow(ax, ay)
      return FillStatus::Stop if [CellType::Rock].includes? cell_at(ax, ay)
      return FillStatus::Drop unless [CellType::WaterRest, CellType::Rock].includes? cell_at(ax, ay + 1)
      return FillStatus::Continues
    end

    def initialize(@minx, @maxx, @miny, @maxy, lines)
      @width = @maxx - @minx + 1
      @height = @maxy - @miny + 1
      @data = Array(CellType).new(@width*@height, CellType::Empty)
      lines.each do |line|
        line.draw do |x, y|
          set_cell(x - @minx, y - @miny, CellType::Rock)
        end
      end
      Flow.new(self, 500 - @minx, {0, 0 - @miny}.max)
    end

    def draw
      String.build do |str|
        @height.times do |y|
          @width.times do |x|
            str << CELL_SYM[cell_at(x, y)]
          end
          str << "\n"
        end
      end
    end

    def simulate_flows
      while @flows.size > 0
        flow = @flows.shift
        flow.simulate
      end
    end

    def rate
      @data.count { |x| x.water_flow? || x.water_rest? }
    end

    def rate2
      @data.count { |x| x.water_rest? }
    end
  end

  def part1(input)
    lines = input.chomp.split('\n').map { |s| Line.new(s) }
    minx = lines.min_of { |line| line.x1 }
    maxx = lines.max_of { |line| line.x2 }
    miny = lines.min_of { |line| line.y1 }
    maxy = lines.max_of { |line| line.y2 }
    map = Map.new(minx - 1, maxx + 1, miny, maxy, lines)
    map.simulate_flows
    # puts ""
    # puts map.draw
    map.rate
  end

  def test_draw(input)
    lines = input.chomp.split('\n').map { |s| Line.new(s) }
    minx = lines.min_of { |line| line.x1 }
    maxx = lines.max_of { |line| line.x2 }
    miny = lines.min_of { |line| line.y1 }
    maxy = lines.max_of { |line| line.y2 }
    map = Map.new(minx - 1, maxx + 1, miny, maxy, lines)
    map.simulate_flows
    # puts ""
    # puts map.draw
    map.draw
  end

  def part2(input)
    lines = input.chomp.split('\n').map { |s| Line.new(s) }
    minx = lines.min_of { |line| line.x1 }
    maxx = lines.max_of { |line| line.x2 }
    miny = lines.min_of { |line| line.y1 }
    maxy = lines.max_of { |line| line.y2 }
    map = Map.new(minx - 1, maxx + 1, miny, maxy, lines)
    map.simulate_flows
    map.rate2
  end
end
