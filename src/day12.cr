require "./aoc2018"

module Day12
  include Day
  extend self

  class Board
    getter plants : Deque(Bool)
    @nextplants : Deque(Bool)
    property zeroindex = 0_i64
    getter rules : StaticArray(Bool, 32)
    @tick = 0
    getter static = false

    def initialize(startpos)
      @plants = Deque(Bool).new
      startpos.each_char do |c|
        @plants << (c == '#')
      end
      # print
      @nextplants = @plants.clone
      @rules = StaticArray(Bool, 32).new(false)
    end

    def set_rule(rule)
      raise "incorrect rule #{rule}" unless m = /(.....) => (.)/.match(rule)
      v = m[1].chars.map { |x| x == '#' }
      @rules[get_index(v)] = m[2] == "#"
    end

    def get_index(values)
      n = 0
      5.times do |i|
        n += (1 << (4 - i)) if values[i]
      end
      n
    end

    def step
      @tick += 1
      4.times { @plants.push false }
      4.times { @plants.unshift false }
      (@plants.size - @nextplants.size).times { @nextplants << false }
      @zeroindex += 4
      2.times { |i| @nextplants[i] = false }
      2.times { |i| @nextplants[-i - 1] = false }
      n = 2
      @plants.each_cons(5, true) do |item|
        @nextplants[n] = rules[get_index(item)]
        n += 1
      end

      while @nextplants.size > 0 && !@nextplants.first
        @nextplants.shift
        @zeroindex -= 1
      end

      while @nextplants.size > 0 && !@nextplants.last
        @nextplants.pop
      end

      # compare after clean
      while @plants.size > 0 && !@plants.first
        @plants.shift
      end

      while @plants.size > 0 && !@plants.last
        @plants.pop
      end

      if @plants.size == @nextplants.size
        same = true
        @nextplants.each_with_index do |v, i|
          if @plants[i] != @nextplants[i]
            same = false
            break
          end
        end
        @static = true if same
      end

      tmp = @nextplants
      @nextplants = @plants
      @plants = tmp
      # print
    end

    def rate
      sum = 0_i64
      plants.each_with_index do |v, i|
        sum = sum + i - zeroindex if v
      end
      sum
    end

    def print
      puts(String.build do |str|
        str << 100 + @tick
        (3 - @zeroindex).times { str << "-" }
        plants.each { |c| str << (c ? '#' : '.') }
      end)
    end
  end

  def simulate(input, n)
    list = input.chomp.split("\n")
    raise "incorrect start pos #{list[0]}" unless m = /initial state: ([.#]*)/.match(list[0])
    zone = Board.new(m[1])
    list.skip(2).each do |s|
      zone.set_rule s
    end
    n.times { zone.step }
    zone.rate
  end

  def simulate_checked(input, n)
    list = input.chomp.split("\n")
    raise "incorrect start pos #{list[0]}" unless m = /initial state: ([.#]*)/.match(list[0])
    zone = Board.new(m[1])
    list.skip(2).each do |s|
      zone.set_rule s
    end
    stopped = false
    n.times do |i|
      oldzero = zone.zeroindex
      zone.step
      if zone.static
        deltazero = zone.zeroindex - oldzero
        zone.zeroindex += (n - i - 1)*deltazero
        stopped = true
        break
      end
    end
    {zone.rate, stopped}
  end

  def part1(input)
    simulate(input, 20)
  end

  def part2(input)
    simulate_checked(input, 50000000000)
  end
end
