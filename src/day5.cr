require "./aoc2018"

module Day5
  include Day
  extend self

  class LinkedList
    property! prev : LinkedList
    property next : LinkedList?
    getter letter : Char

    def initialize(@letter)
      @next = nil
      @prev = self
    end

    def initialize(@prev : LinkedList, @letter)
      @next = nil
      prev.next = self
    end

    def other_char
      if @letter.ascii_lowercase?
        @letter.upcase { |x| return x }
      else
        @letter.downcase { |x| return x }
      end
    end

    def drop
      if n = @next
        n.prev = prev
      end
      prev.next = @next
    end

    def process : LinkedList?
      return nil unless n = @next
      if n.letter == other_char
        n.drop
        drop
        return @prev
      else
        return n
      end
    end

    def inspect(io)
      io << @letter
      if n = @next
        n.inspect(io)
      end
    end
  end

  def part1(input)
    root = LinkedList.new('@')
    cur = root
    input.each_char do |x|
      cur = LinkedList.new(cur, x)
    end
    cur = root
    while cur
      cur = cur.process
    end
    n = 0
    cur = root
    while cur
      n += 1
      cur = cur.next
    end
    n - 1
  end

  def part2(input)
    counts = ('a'..'z').map do |bad|
      bad2 = bad
      bad.upcase { |x| bad2 = x }
      root = LinkedList.new('@')
      cur = root
      input.each_char do |x|
        next if x == bad || x == bad2
        cur = LinkedList.new(cur, x)
      end
      cur = root
      while cur
        cur = cur.process
      end
      n = 0
      cur = root
      while cur
        n += 1
        cur = cur.next
      end
      n - 1
    end
    counts.min
  end
end
