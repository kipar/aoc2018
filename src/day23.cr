require "./aoc2018"

module Day23
  include Day
  extend self

  record(Bot, x : Int32, y : Int32, z : Int32, r : Int32) do
    def range(other : Bot)
      (other.x - x).abs + (other.y - y).abs + (other.z - z).abs
    end

    def initialize(line)
      _, ax, ay, az, ar = /pos=<(.*),(.*),(.*)>, r=(.*)/.match(line).not_nil!
      @x, @y, @z, @r = {ax, ay, az, ar}.map(&.to_i)
    end
  end

  def part1(input)
    list = input.chomp.split("\n").map { |s| Bot.new(s) }
    bestbot = list.max_by(&.r)
    list.count { |b| b.range(bestbot) <= bestbot.r }
  end

  def part2(input)
    "part2"
  end
end
