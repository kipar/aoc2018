require "./aoc2018"

module Day14
  include Day
  extend self

  def part1(input)
    recipes = [3_u8, 7_u8]
    elves = [0, 1]
    n = input.chomp.to_i
    loop do
      # make new recipe
      rec = elves.map { |i| recipes[i] }.sum
      if rec >= 10
        recipes << 1
        recipes << rec % 10
      else
        recipes << rec
      end
      break if recipes.size >= n + 10
      # elves pick new items
      elves = elves.map do |i|
        (i + 1 + recipes[i]) % recipes.size
      end
    end
    # now count final number
    recipes[n, 10].map(&.to_s).join
  end

  def part2(input)
    recipes = [3_u8, 7_u8]
    elves = [0, 1]
    n = input.chomp.bytes.size
    scan = input.chomp.split("").map(&.to_i)
    loop do
      # make new recipe
      rec = recipes[elves[0]] + recipes[elves[1]]
      if rec >= 10
        recipes << 1
        break if recipes.size > n && recipes[-n, n] == scan
        recipes << rec % 10
      else
        recipes << rec
      end
      break if recipes.size > n && recipes[-n, n] == scan
      # elves pick new items
      elves.map! do |i|
        (i + 1 + recipes[i]) % recipes.size
      end
    end
    recipes.size - n
  end
end
