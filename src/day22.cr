require "./aoc2018"

module Day22
  include Day
  extend self

  KY0         = 16807
  KX0         = 48271
  EROMOD      = 20183
  SWITCH_TIME =     7

  EROMAP = {} of {Int32, Int32} => Int32

  def erolevel(x, y, depth)
    if memo = EROMAP[{x, y}]?
      return memo
    end
    geoindex = if x == 0 && y == 0
                 0
               elsif y == 0
                 x * KY0
               elsif x == 0
                 y * KX0
               else
                 erolevel(x - 1, y, depth) * erolevel(x, y - 1, depth)
               end
    v = (geoindex + depth) % EROMOD
    EROMAP[{x, y}] = v
    v
  end

  def parse_input(input)
    first, second = input.split("\n")
    _, d = /depth: (\d*)/.match(first).not_nil!
    _, tx, ty = /target: (\d*),(\d*)/.match(second).not_nil!
    {tx.to_i, ty.to_i, d.to_i}
  end

  def part1(input)
    tx, ty, depth = parse_input(input.chomp)
    erolevel(tx, ty, depth)
    EROMAP[{tx, ty}] = depth
    EROMAP.values.sum { |v| v % 3 }
  end

  enum Terrain
    Rocky
    Wet
    Narrow
  end

  enum Tool
    Neither
    Torch
    ClimbingGear
  end

  class Surface
    def initialize(@depth : Int32)
    end

    def terrain(x, y)
      Terrain.new(Day22.erolevel(x, y, @depth) % 3)
    end

    LOCKED_TOOLS = {
      Terrain::Rocky  => Tool::Neither,
      Terrain::Wet    => Tool::Torch,
      Terrain::Narrow => Tool::ClimbingGear,
    }

    def allow(x, y, tool)
      tool != LOCKED_TOOLS[terrain(x, y)]
    end
  end

  record(Position, x : Int32, y : Int32, tool : Tool) do
    def possible_moves(surface, &block)
      [{-1, 0}, {1, 0}, {0, -1}, {0, 1}].each do |dx, dy|
        ax = x + dx
        next if ax < 0
        ay = y + dy
        next if ay < 0
        yield Position.new(ax, ay, tool) if surface.allow(ax, ay, tool)
      end
    end

    def possible_switches(surface, &block)
      Tool.values.each do |atool|
        next if atool == tool
        yield Position.new(x, y, atool) if surface.allow(x, y, atool)
      end
    end
  end

  def part2(input)
    tx, ty, depth = parse_input(input.chomp)
    surface = Surface.new(depth)
    start = Position.new(0, 0, Tool::Torch)
    finish = Position.new(tx, ty, Tool::Torch)
    wave = {} of Position => Int32
    done = Set(Position).new
    wave[start] = 0
    tick = 0
    to_add = [] of Position
    to_remove = [] of Position
    loop do
      tick += 1
      to_add.clear
      to_remove.clear
      wave.each do |pos, time|
        case time
        when tick - 1
          pos.possible_moves(surface) do |newpos|
            to_add << newpos
          end
        when tick - SWITCH_TIME
          pos.possible_switches(surface) do |newpos|
            to_add << newpos
            to_remove << pos
          end
        end
      end
      # pp! tick, to_add, to_remove
      to_remove.each do |p|
        done.add(p)
        wave.delete(p)
      end
      to_add.each do |p|
        return tick if p == finish
        next if done.includes? p
        next if wave[p]?
        wave[p] = tick
      end
    end
  end
end
