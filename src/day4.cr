require "./aoc2018"

module Day4
  include Day
  extend self

  NMINUTES = 60

  enum EventType
    Begins
    Asleep
    WakeUp
  end

  record(Event, date : String, minute : Int32, typ : EventType, guard : Int32) do
    include Comparable(Event)

    def initialize(s : String)
      raise "incorrect event" unless m = /\[(.*) (\d\d):(\d\d)\] (.*)/.match(s)
      @date, hour, aminute, text = {m[1], m[2], m[3], m[4]}
      @minute = aminute.to_i + hour.to_i*60
      @guard = 0
      @typ = case text.strip
             when "wakes up"
               EventType::WakeUp
             when "falls asleep"
               EventType::Asleep
             else
               raise "incorrect event" unless m = /Guard #(\d*) begins shift/.match(text)
               @guard = m[1].to_i
               EventType::Begins
             end
    end

    def <=>(other : Event)
      if @date == other.date
        minute <=> other.minute
      else
        @date <=> other.date
      end
    end
  end

  class HourData
    @data = {} of Int32 => Hash(String, Array(Bool))
    @cur_guard = 0

    def add_record(line : Event)
      case line.typ
      when EventType::Begins
        @cur_guard = line.guard
        @data[@cur_guard] = Hash(String, Array(Bool)).new unless @data.has_key?(@cur_guard)
      when EventType::Asleep, EventType::WakeUp
        journal = @data[@cur_guard]
        journal[line.date] = Array(Bool).new(NMINUTES, false) unless journal.has_key?(line.date)
        list = journal[line.date]
        (line.minute..NMINUTES - 1).each { |x| list[x] = line.typ == EventType::Asleep }
      end
    end

    def print
      @data.each do |guard, journal|
        puts "guard: #{guard}"
        journal.each do |date, list|
          liststr = list.map { |x| x ? "#" : "." }
          puts "#{date}: #{liststr.join}"
        end
      end
    end

    def part1_guard
      worst_guard = @data.keys.max_by { |g| @data[g].values.sum { |list| list.sum { |x| x ? 1 : 0 } } }
      journal = @data[worst_guard]
      sleepy_time = (1..NMINUTES - 1).max_by { |m| journal.values.sum { |list| list[m] ? 1 : 0 } }
      worst_guard*sleepy_time
    end

    def part2_guard
      worst_guard = 0
      worst_time = 0
      worst_minute = 0
      (1..NMINUTES - 1).each do |m|
        guard = @data.keys.max_by { |g| @data[g].values.sum { |list| list[m] ? 1 : 0 } }
        count = @data[guard].values.sum { |list| list[m] ? 1 : 0 }
        if count > worst_time
          worst_time = count
          worst_minute = m
          worst_guard = guard
        end
      end
      worst_guard*worst_minute
    end
  end

  def part1(input)
    data = HourData.new
    events = input.chomp.split("\n").map { |s| Event.new(s) }.sort
    events.each { |x| data.add_record x }
    data.part1_guard
  end

  def part2(input)
    data = HourData.new
    events = input.chomp.split("\n").map { |s| Event.new(s) }.sort
    events.each { |x| data.add_record x }
    data.part2_guard
  end
end
