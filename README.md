# aoc2018

Solutions for [Advent of Code 2018](https://adventofcode.com/2018)
My goal is clean (with all tests and without too much crunches) code that is general enough and without unnecessary performance degradation.
`crystal spec` - to run tests, `crystal run.cr --release` to solve problems for inputs from `input` directory.
